#!/usr/bin/env bash

print_usage() {
  echo "Please enter the environment as argument for this command."
  echo "Usage: ./deploy_mysql_kubernetes.sh -e ENVIRONMENT"
  echo "Example: ./deploy_mysql_kubernetes.sh -e dev"
  exit 1
}

# the first ':' is to disable the default error handling of invalid options.
# the second and third ':' are indicating that options 'p' and 'n' require arguments.
while getopts ":e:" options; do
  case "${options}" in
    e)
      export ENVIRONMENT=${OPTARG}
      ;;
    :)
      echo "Error: -${OPTARG} requires an argument."
      print_usage
      ;;
    \?)
    #*) # Same effect as \?)
      echo "Error: Invalid option -${OPTARG}."
      print_usage
      ;;
  esac
done
# Removed options arguments
shift $((OPTIND -1))

if [ -z "${ENVIRONMENT}" ]; then
  print_usage
  exit 1
fi

: "${WORKSPACE:=$(pwd)}"

helm repo add bitnami https://charts.bitnami.com/bitnami
cd "${WORKSPACE}"/helm/mysql || exit

CURRENT_CONTEXT=$(kubectl config current-context)
echo "You are deploying into the ${CURRENT_CONTEXT} cluster..."

envsubst < values.mysql.yaml.tpl > values.mysql.yaml

kubectl create namespace onboarding-app-"${ENVIRONMENT}"
helm upgrade --install \
  mysql bitnami/mysql \
  --wait \
  --timeout="15m" \
  --namespace onboarding-app-"${ENVIRONMENT}" \
  -f values.mysql.yaml
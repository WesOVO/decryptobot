FROM mysql:8.0.24

COPY ./scripts/ /docker-entrypoint-initdb.d/
COPY ./mysql.cnf /etc/mysql/conf.d/custom.cnf
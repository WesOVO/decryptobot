#!/usr/bin/env bash

print_usage() {
  echo "Please enter the docker image tag as arguments for this command."
  echo "Usage: ./build_docker.sh -i IMAGE_TAG"
  echo "Example: ./build_docker.sh -e dev -i build-123456"
  exit 1
}

# the first ':' is to disable the default error handling of invalid options.
# the second and third ':' are indicating that options 'p' and 'n' require arguments.
while getopts ":i:" options; do
  case "${options}" in
    i)
      export IMAGE_TAG=${OPTARG}
      ;;
    :)
      echo "Error: -${OPTARG} requires an argument."
      print_usage
      ;;
    \?)
    #*) # Same effect as \?)
      echo "Error: Invalid option -${OPTARG}."
      print_usage
      ;;
  esac
done
# Removed options arguments
shift $((OPTIND -1))

if [ -z "${IMAGE_TAG}" ]; then
  print_usage
  exit 1
fi

docker-compose build
# docker-compose push

const createGroup = (_groupId, _groupName) => {
  const groupId = _groupId;
  const groupName = _groupName;

  return {
    getGroupId: () => groupId,
    getGroupName: () => groupName,
  };
};

module.exports = { createGroup };

const GameList = require('./gameList');
const Game = require('./game');
const Group = require('./group');
const Counter = require('./countDown');
const db = require('./database');

const gameList = GameList.getInstance();

const insertUser = async (id, name) => {
  const query = await db.query('INSERT into User (id,name) VALUES (?,?)', [
    id,
    name,
  ]);
  return query[0];
};

const queryDb = async (id) => {
  const query = await db.query(
    'SELECT EXISTS(SELECT 1 FROM User WHERE id = ? LIMIT 1) AS exist',
    id,
  );
  return query[0];
};

const startGame = async (msg, bot) => {
  const result = await queryDb(msg.from.id);
  if (msg.chat.type === 'group' || msg.chat.type === 'supergroup') {
    if (!result[0].exist) {
      bot.sendMessage(
        msg.chat.id,
        'Please start me first before joining game!',
        {
          reply_to_message_id: msg.message_id,
          reply_markup: {
            inline_keyboard: [
              [
                {
                  text: 'Press me to start',
                  url: 'http://t.me/decryptoOVO_bot',
                },
              ],
            ],
          },
        },
      );
    } else if (!gameList.getByGroupId(msg.chat.id)) {
      const group = Group.createGroup(msg.chat.id, msg.chat.title);
      const game = Game.createGame(group, bot);
      const counter = Counter.createCounter(group, game, bot);
      game.registerCounter(counter);
      gameList.insertGame(msg.chat.id, msg.from.id, game);
      if (game.addPlayer(msg.from.id, msg.from.first_name)) {
        bot.sendMessage(
          msg.chat.id,
          `${
            msg.from.first_name
          } has joined the game! ${game.getPlayerCount()} has joined.`,
        );
      }
    } else {
      const game = gameList.getByPlayerId(msg.from.id);
      if (!game) {
        const runningGame = gameList.getByGroupId(msg.chat.id);
        runningGame.addPlayer(msg.from.id, msg.from.first_name);
        gameList.insertPlayer(msg.from.id, runningGame);
        bot.sendMessage(
          msg.chat.id,
          `${
            msg.from.first_name
          } Join左！而家有${runningGame.getPlayerCount()}條友`,
        );
        return;
      }
      if (game.getStatus() === 'pending') {
        bot.sendMessage(msg.chat.id, 'You have already joined.');
      }
    }
  } else if (!result[0].exist) {
    try {
      const insertResult = await insertUser(
        msg.from.id,
        msg.from.user_name,
        bot,
      );
      if (insertResult.affectedRows === 1) {
        bot.sendMessage(msg.chat.id, '撻著左喇！去玩啦！');
      }
    } catch (err) {
      console.log(err);
    }
  }
};
const joinGame = async (msg, bot) => {
  const result = await queryDb(msg.from.id);
  if (!result[0].exist) {
    bot.sendMessage(msg.chat.id, 'Please start me first before joining game!', {
      reply_to_message_id: msg.message_id,
      reply_markup: {
        inline_keyboard: [
          [
            {
              text: 'Press me to start',
              url: 'http://t.me/decryptoOVO_bot',
            },
          ],
        ],
      },
    });
  } else if (msg.chat.type === 'group' || msg.chat.type === 'supergroup') {
    const game = gameList.getByGroupId(msg.chat.id);
    if (!game) {
      bot.sendMessage(msg.chat.id, 'Type /start to start!');
    } else if (!gameList.getByPlayerId(msg.from.id)) {
      game.addPlayer(msg.from.id, msg.from.first_name);
      gameList.insertPlayer(msg.from.id, game);
      bot.sendMessage(
        msg.chat.id,
        `${msg.from.first_name} Join左！而家有${game.getPlayerCount()}條友`,
      );
    }
  }
};
const fleeGame = (msg, bot) => {
  if (msg.chat.type === 'group' || msg.chat.type === 'supergroup') {
    const game = gameList.getByGroupId(msg.chat.id);
    if (!game) {
      bot.sendMessage(msg.chat.id, '冇人開game番而家。用/start黎開啦！');
    } else if (game.getStatus() === 'pending') {
      if (game.removePlayer(msg.from.id)) {
        gameList.removePlayer(msg.from.id, game);
        bot.sendMessage(msg.chat.id, `${msg.from.first_name} 走咗!`);
      }
    }
  }
};

const encryptMessage = (msg) => {
  const game = gameList.getByPlayerId(msg.from.id);
  if (game) {
    game.encryptMessage(msg);
  }
};

const decryptMessage = (msg) => {
  const game = gameList.getByPlayerId(msg.from.id);
  if (game) {
    game.decryptMessage(msg);
  }
};

const receiveMessage = (msg) => {
  const game = gameList.getByPlayerId(msg.from.id);
  if (game) {
    game.receiveMessage(msg);
  }
};
const extendTimer = (msg, bot) => {
  const game = gameList.getByGroupId(msg.chat.id);
  if (!game) {
    bot.sendMessage(
      msg.chat.id,
      'No game running in this group. Why not start a new one?',
    );
  } else {
    game.extendTimer();
  }
};
const forceStartGame = (msg) => {
  const game = gameList.getByPlayerId(msg.from.id);
  if (game) {
    game.forceStart(msg);
  }
};

const forceStopGame = (msg) => {
  const game = gameList.getByPlayerId(msg.from.id);
  if (game) {
    game.forceStop(msg);
  }
};

module.exports = {
  startGame,
  fleeGame,
  joinGame,
  extendTimer,
  encryptMessage,
  decryptMessage,
  receiveMessage,
  forceStartGame,
  forceStopGame,
};

const createPlayer = (_id, _name) => {
  const id = _id;
  const name = _name;

  return {
    getName: () => name,
    getPlayerId: () => id,
  };
};

module.exports = { createPlayer };

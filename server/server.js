const dotenv = require('dotenv');
const TelegramBot = require('node-telegram-bot-api');

const Command = require('./command');

dotenv.config();

const url = process.env.NGROK_URL;
const TOKEN = process.env.BOT_TOKEN;

const bot = new TelegramBot(TOKEN, { polling: true });

bot.setWebHook(`${url}/bot${TOKEN}`);

bot.onText(/\/start/, (msg) => {
  Command.startGame(msg, bot);
});

bot.onText(/\/code/, (msg) => {
  Command.encryptMessage(msg, bot);
});

bot.onText(/\/decrypt/, (msg) => {
  Command.decryptMessage(msg, bot);
});

bot.onText(/\/receive/, (msg) => {
  Command.receiveMessage(msg, bot);
});

bot.onText(/\/flee/, (msg) => {
  Command.fleeGame(msg, bot);
});

bot.onText(/\/join/, (msg) => {
  Command.joinGame(msg, bot);
});

bot.onText(/\/extend/, (msg) => {
  Command.extendTimer(msg, bot);
});

bot.onText(/\/forcestart/, (msg) => {
  Command.forceStartGame(msg);
});

bot.onText(/\/forcestop/, (msg) => {
  Command.forceStopGame(msg);
});

bot.on('callback_query', (msg) => {
  Command.handleCallBack(msg);
});

bot.on('message', (msg) => {
  console.log(msg);
});

bot.on('new_chat_members', (msg) => {
  if (msg.new_chat_participant.id.toString() === process.env.BOT_ID) {
    bot.sendMessage(msg.chat.id, 'Hi guys. I am Decryto Bot');
    bot.sendMessage(
      process.env.ADMIN_ID,
      `New group invitation:\nGroup name:${msg.chat.title}\nOperator:${msg.from.first_name}\nTgId:<a href="tg://user?id=${msg.from.id}"> ${msg.from.username} </a>`,
      { parse_mode: 'HTML' },
    );
  }
});
bot.on('polling_error', console.log);

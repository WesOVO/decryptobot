const createCounter = (_group, _game, _bot) => {
  const group = _group;
  const game = _game;
  const bot = _bot;
  let time = 120;

  const timer = setInterval(() => {
    time -= 1;
    if (time === 0) {
      clearTimeout(timer);
      timer.unref();
      game.notifyTimesUp();
      return;
    }
    if (time % 30 === 0) {
      bot.sendMessage(group.getGroupId(), `仲有${time}秒！`);
    }
  }, 1000);

  let inGameTimer;

  return {
    extend: () => {
      time += 30;
      bot.sendMessage(
        group.getGroupId(),
        `俾多30秒你地。拿拿臨搵夠人Join Game！\n仲有${time}秒！`,
      );
    },
    kill: () => {
      clearTimeout(timer);
      timer.unref();
    },
    killInGame: () => {
      clearTimeout(inGameTimer);
      inGameTimer.unref();
    },
    start: () => {
      time = 400;
      inGameTimer = setInterval(() => {
        time -= 1;
        if (time === 0) {
          clearTimeout(inGameTimer);
          inGameTimer.unref();
          game.noAction();
          return;
        }
        if (time < 120 && time % 30 === 0) {
          bot.sendMessage(group.getGroupId(), `仲有${time}秒進行討論！`);
        }
      }, 1000);
    },
  };
};

module.exports = { createCounter };

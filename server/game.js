const GameList = require('./gameList.js');
const Player = require('./player');
const util = require('./util');

const gameList = GameList.getInstance();

const createGame = (_group, _bot) => {
  const group = _group;
  const playerList = [];
  let teamA;
  let teamB;
  const bot = _bot;
  let status = 'pending';
  let counter;
  let teamTurn = 0;
  const playerMaxCount = 2;
  let captainA = 0;
  let captainB = 0;
  const voteToForceStart = [];
  const voteToForceStop = [];
  const token = {
    0: {
      success: 0,
      failure: 0,
    },
    1: {
      success: 0,
      failure: 0,
    },
  };
  const keywords = [
    '噴泉',
    '天使',
    '老母',
    '老豆',
    '炸雞',
    '炸蝦',
    '炸彈',
    '炸豆腐',
  ];
  const keywordsA = [];
  const keywordsB = [];
  const answer = ['1', '2', '3', '4'];

  const terminate = () => {
    gameList.removeGame(group.getGroupId(), playerList);
    if (counter) {
      counter.kill();
    }
  };

  const notifyCaptain = async () => {
    await util.shuffleArray(answer);
    const copyAns = answer.slice();
    copyAns.splice(3, 1);
    let captainId;
    if (teamTurn === 0) {
      captainId = teamA[captainA].getPlayerId();
    } else {
      captainId = teamB[captainB].getPlayerId();
    }
    bot.sendMessage(captainId, `你地今次嘅密碼係\n${copyAns.join(' ')}`);
    bot.sendMessage(
      captainId,
      '將你嘅提示打係輸入欄\n格式係：\n/code 提示1 提示2 提示3',
    );
  };

  const endTurn = async () => {
    if (token[teamTurn].failure === 2) {
      bot.sendMessage(
        group.getGroupId(),
        `遊戲結束\n${teamTurn === 0 ? '紅' : '藍'}隊有${
          token[teamTurn].failure
        }個\u{26AB}所以輸咗！`,
        { parse_mode: 'HTML' },
      );
      terminate();
      return;
    }
    if (token[(teamTurn + 1) % 2].success === 2) {
      bot.sendMessage(
        group.getGroupId(),
        `遊戲結束\n${teamTurn === 0 ? '藍' : '紅'}隊有${
          token[(teamTurn + 1) % 2].success
        }個\u{26AA}所以贏咗！`,
        { parse_mode: 'HTML' },
      );
      terminate();
      return;
    }

    bot.sendMessage(
      group.getGroupId(),
      `總結而時結果\n紅隊有${token[0].success}個\u{26AA} ${token[0].failure}個\u{26AB}\n藍隊有${token[1].success}個\u{26AA} ${token[1].failure}個\u{26AB}\n比賽繼續！交換攻守！`,
      { parse_mode: 'HTML' },
    );
    captainA = (captainA + 1) % teamA.length;
    captainB = (captainB + 1) % teamB.length;
    teamTurn = (teamTurn + 1) % 2;
    let captain;
    let teamAString = '';
    let teamBString = '';
    if (teamTurn === 0) {
      captain = teamA[captainA];
    } else {
      captain = teamB[captainB];
    }
    teamA.forEach((player, index) => {
      if (index === captainA) teamAString += `<a href="tg://user?id=${player.getPlayerId()}">${player.getName()}</a> \u{1F451}\n`;
      else teamAString += `<a href="tg://user?id=${player.getPlayerId()}">${player.getName()}</a>\n`;
    });

    teamB.forEach((player, index) => {
      if (index === captainB) teamBString += `<a href="tg://user?id=${player.getPlayerId()}">${player.getName()}</a> \u{1F451}\n`;
      else teamBString += `<a href="tg://user?id=${player.getPlayerId()}">${player.getName()}</a>\n`;
    });

    bot.sendMessage(
      group.getGroupId(),
      `交換隊長！\n\n紅隊：\n${teamAString}\n\n藍隊：\n${teamBString}\n\n而家等${
        teamTurn === 0 ? '紅' : '藍'
      }隊嘅隊長 <a href="tg://user?id=${captain.getPlayerId()}"> ${captain.getName()} </a> 加密佢信息`,
      { parse_mode: 'HTML' },
    );
    await util.wait(1000);
    notifyCaptain();
  };

  const notifyAll = async () => {
    let teamAString = '';
    let teamBString = '';
    teamA.forEach((player) => {
      teamAString += `<a href="tg://user?id=${player.getPlayerId()}">${player.getName()}</a> `;
    });
    teamB.forEach((player) => {
      teamBString += `<a href="tg://user?id=${player.getPlayerId()}">${player.getName()}</a> `;
    });

    bot.sendMessage(
      group.getGroupId(),
      `請雙方進行討論\n${teamAString}${teamBString}\n麻煩${
        teamTurn === 0 ? '紅' : '藍'
      }隊嘅隊長收皮！`,
      { parse_mode: 'HTML' },
    );

    let captain;
    if (teamTurn === 0) {
      captain = teamB[captainB];
    } else {
      captain = teamA[captainA];
    }
    await util.wait(500);
    bot.sendMessage(
      group.getGroupId(),
      `有結果之後由<a href="tg://user?id=${captain.getPlayerId()}"> ${captain.getName()} </a>負責將自己隊嘅討論結果講我知！\n`,
      { parse_mode: 'HTML' },
    );
    bot.sendMessage(
      captain.getPlayerId(),
      '將你地嘅解密打係下面輸入欄\n格式係：\n/decrypt 1 2 3',
    );
  };

  const handleReceiveMessage = async (msg) => {
    const parse = msg.text.split(' ');
    let captainId;
    if (teamTurn === 0) {
      captainId = teamA[(captainA + 1) % teamA.length].getPlayerId();
    } else {
      captainId = teamB[(captainB + 1) % teamB.length].getPlayerId();
    }
    if (msg.from.id === captainId) {
      if (parse.length !== 4) {
        bot.sendMessage(captainId, '格式唔啱喎 你check check啦');
      } else {
        parse.splice(0, 1);

        if (
          parse[0] === answer[0]
          && parse[1] === answer[1]
          && parse[2] === answer[2]
        ) {
          bot.sendMessage(
            group.getGroupId(),
            `${
              teamTurn === 0 ? '紅' : '藍'
            }隊內部成功解密！\n密碼係${parse.join(' ')}`,
          );
        } else {
          const copyAns = answer.slice();
          copyAns.splice(3, 1);
          bot.sendMessage(
            group.getGroupId(),
            `${
              teamTurn === 0 ? '紅' : '藍'
            }隊內部解密失敗阿！！！\n密碼係${copyAns.join(' ')}`,
          );
          token[teamTurn].failure += 1;
        }
        await util.wait(1000);
        endTurn();
      }
    }
  };
  const handleDecryptMessage = async (msg) => {
    const parse = msg.text.split(' ');
    let captainId;
    if (teamTurn === 0) {
      captainId = teamB[captainB].getPlayerId();
    } else {
      captainId = teamA[captainA].getPlayerId();
    }
    if (msg.from.id === captainId) {
      if (parse.length !== 4) {
        bot.sendMessage(captainId, '格式唔啱喎 你check check啦');
      } else {
        parse.splice(0, 1);

        if (
          parse[0] === answer[0]
          && parse[1] === answer[1]
          && parse[2] === answer[2]
        ) {
          bot.sendMessage(
            group.getGroupId(),
            `${
              teamTurn === 0 ? '藍' : '紅'
            }隊成功破解密碼！\n密碼係${parse.join(' ')}`,
          );
          token[(teamTurn + 1) % 2].success += 1;
          endTurn();
        } else {
          bot.sendMessage(
            group.getGroupId(),
            `${teamTurn === 0 ? '藍' : '紅'}隊諗住用${parse.join(
              ' ',
            )}黎破解\n但係失敗阿！！！`,
          );

          let captain;

          if (teamTurn === 0) {
            captain = teamA[(captainA + 1) % teamA.length];
          } else {
            captain = teamB[(captainB + 1) % teamB.length];
          }
          await util.wait(500);
          bot.sendMessage(
            group.getGroupId(),
            `交番個場俾${
              teamTurn === 0 ? '紅' : '藍'
            }隊\n有結果之後由<a href="tg://user?id=${captain.getPlayerId()}"> ${captain.getName()} </a>負責將討論結果講我知！\n`,
            { parse_mode: 'HTML' },
          );
          bot.sendMessage(
            captain.getPlayerId(),
            '將你地嘅解密打係下面輸入欄\n格式係：\n/receive 1 2 3',
          );
        }
      }
    }
  };

  const handleEncryptMessage = (msg) => {
    const parse = msg.text.split(' ');
    let captainId;
    if (teamTurn === 0) {
      captainId = teamA[captainA].getPlayerId();
    } else {
      captainId = teamB[captainB].getPlayerId();
    }
    if (msg.from.id === captainId) {
      if (parse.length !== 4) {
        bot.sendMessage(captainId, '格式唔啱喎 你check check啦');
      } else {
        parse.splice(0, 1);
        bot.sendMessage(
          group.getGroupId(),
          `${teamTurn === 0 ? '紅' : '藍'}隊嘅提示係\n${parse.join(' ')}`,
        );
        notifyAll();
      }
    }
  };

  const init = async () => {
    status = 'running';
    if (counter) {
      counter.kill();
    }
    let teamAString = '';
    let teamBString = '';
    util.shuffleArray(playerList);
    teamA = playerList.slice(0, Math.floor(playerList.length / 2));
    teamB = playerList.slice(Math.floor(playerList.length / 2));
    keywordsA.push(...keywords.splice(0, 4));
    keywordsB.push(...keywords.splice(0));

    teamA.forEach((player, index) => {
      if (index === captainA) teamAString += `<a href="tg://user?id=${player.getPlayerId()}">${player.getName()}</a> \u{1F451}\n`;
      else teamAString += `<a href="tg://user?id=${player.getPlayerId()}">${player.getName()}</a>\n`;
    });

    teamB.forEach((player, index) => {
      if (index === captainB) teamBString += `<a href="tg://user?id=${player.getPlayerId()}">${player.getName()}</a> \u{1F451}\n`;
      else teamBString += `<a href="tg://user?id=${player.getPlayerId()}">${player.getName()}</a>\n`;
    });

    bot.sendMessage(
      group.getGroupId(),
      `遊戲開始\n\n紅隊嘅人馬有：\n${teamAString}\n\n藍隊嘅人馬有：\n${teamBString}我而家會將關鍵字私底下講俾大家知！`,
      { parse_mode: 'HTML' },
    );

    teamA.forEach((player) => {
      bot.sendMessage(
        player.getPlayerId(),
        `你係紅隊\n呢啲係你嘅隊友\n${teamBString}\n\n你地隊嘅關鍵字係\n${keywordsA.join(
          ' ',
        )}`,
        { parse_mode: 'HTML' },
      );
    });
    teamB.forEach((player) => {
      bot.sendMessage(
        player.getPlayerId(),
        `你係藍隊\n呢啲係你嘅隊友\n${teamBString}\n\n你地隊嘅關鍵字係\n${keywordsB.join(
          ' ',
        )}`,
        { parse_mode: 'HTML' },
      );
    });
    bot.sendMessage(
      group.getGroupId(),
      `而家等${
        teamTurn === 0 ? '紅' : '藍'
      }隊嘅隊長 <a href="tg://user?id=${teamA[
        captainA
      ].getPlayerId()}"> ${teamA[captainA].getName()} </a> 加密佢信息`,
      { parse_mode: 'HTML' },
    );
    await util.wait(1000);
    notifyCaptain();
  };
  const forceStartGame = (msg) => {
    if (status === 'pending' && !voteToForceStart.includes(msg.from.id)) {
      voteToForceStart.push(msg.from.id);
      bot.sendMessage(
        group.getGroupId(),
        `${msg.from.first_name}想快啲開波！${voteToForceStart.length}/6個人投咗`,
      );
      if (voteToForceStart.length >= 6) {
        init();
      }
    }
  };
  const forceStopGame = (msg) => {
    if (status === 'running' && !voteToForceStop.includes(msg.from.id)) {
      voteToForceStop.push(msg.from.id);
      bot.sendMessage(
        group.getGroupId(),
        `${msg.from.first_name}想提早完場！${
          voteToForceStop.length
        }/${Math.ceil(playerList.length % 2)}個人投咗`,
      );
      if (voteToForceStop.length >= Math.ceil(playerList.length % 2)) {
        terminate();
      }
    }
  };

  return {
    addPlayer: (playerId, playerName) => {
      playerList.push(Player.createPlayer(playerId, playerName));
      bot.sendMessage(playerId, '場已入。');
      if (playerList.length === playerMaxCount) {
        init();
      }
      return true;
    },
    getPlayerCount: () => playerList.length,
    removePlayer: (playerId) => {
      let found;
      playerList.forEach((player, index) => {
        if (player.getPlayerId() === playerId) {
          found = index;
        }
      });
      playerList.splice(found, 1);
      gameList.removePlayer(playerId);
      bot.sendMessage(group.getGroupId(), '有人走咗場！');
      bot.sendMessage(playerId, '場已走。');
      if (playerList.length === 0) {
        terminate();
        bot.sendMessage(group.getGroupId(), '冇人！散水！');
        return false;
      }
      return true;
    },
    getStatus: () => status,
    registerCounter: (aCounter) => {
      counter = aCounter;
    },
    notifyTimesUp: () => {
      if (playerList.length >= 6) {
        init();
      } else {
        bot.sendMessage(group.getGroupId(), '唔夠人。散～水～');
        terminate();
      }
    },

    extendTimer: () => counter.extend(),
    encryptMessage: (msg) => handleEncryptMessage(msg),
    decryptMessage: (msg) => handleDecryptMessage(msg),
    receiveMessage: (msg) => handleReceiveMessage(msg),
    forceStart: (msg) => forceStartGame(msg),
    forceStop: (msg) => forceStopGame(msg),
    // noAction: () => endTurn(),
  };
};

module.exports = { createGame };
